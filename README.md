# windows-remote-powershell

What it does:
- Example of one way files and commands can be deployed to remote windows servers.
- Copies powershell script that creates a folder with a random name in C:\temp.
- Runs the script on the remote server.

Setup:
- gitlab-runner running as a domain account, not system.
- Service account credentials stored as variables. 

Sequence:
- Copy powershell script file.
- Run script on remote server. 


